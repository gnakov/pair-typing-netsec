#!/usr/bin/python3

import os

from flask import Flask
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, jsonify,
    make_response
)

import io
import csv
from string import ascii_lowercase
from random import choice

from . import db

NUMBER_OF_TRIES_PER_PAIR = 5
NUMBER_OF_TRIES = 3
ALL_PAIRS = ([(x,y) for x in ascii_lowercase for y in ascii_lowercase])


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    @app.route('/', methods=['GET'])
    def indexGet():
        return render_template('name.html')

    def getPair(username):

        d = db.get_db()
        pairs = d.execute( 'SELECT DISTINCT pair FROM frequency where username = ? ',
                             (username,) ).fetchall()

        tupled_pairs = [ (p[0][0], p[0][1]) for p in pairs ]

        # no repetion
        pairs =  [p for p in ALL_PAIRS if p not in pairs ]
        pair  = choice(pairs)
        pair  = "%c%c" % (pair)
        session['pairs'] = pair

        return pair

    @app.route('/', methods=['POST'])
    def indexPost():
        session['username'] = request.form.get('input-username')

        pair = getPair(session['username'].lower())
        session['count-pairs'] = NUMBER_OF_TRIES

        return redirect(url_for('type', pair = pair ))


    # a simple page that says hello
    @app.route('/<pair>')
    def type(pair):
        g.pair = pair
        g.displayPair = "(%c %c)" % (pair[0], pair[1])
        g.counter = NUMBER_OF_TRIES_PER_PAIR
        return render_template('typing.html')
        # return 'Going to test user ' + username + ' for ' + pair

    @app.route('/save', methods=['POST'])
    def save():
        durations = request.get_json()
        data = [(session['username'].lower(), session['pairs'], d) for d in durations]

        session['count-pairs'] -= 1

        d = db.get_db()
        for x in data:
            d.execute("INSERT INTO frequency VALUES(NULL, ?, ?, ?, CURRENT_TIMESTAMP)", x)
        d.commit()
        if (session['count-pairs'] > 0):
            # try one more time
            pair = getPair(session['username'].lower())

            return jsonify(url_for('type', pair = pair))
        else:
            return jsonify(url_for('thanks'))

    @app.route('/dump')
    def dump():
        si = io.StringIO()
        cw = csv.writer(si)
        d  = db.get_db()
        data = d.execute("SELECT * from frequency").fetchall()
        cw.writerows(data)
        output = make_response(si.getvalue())
        output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        output.headers["Content-type"] = "text/csv"
        return output

    @app.route('/thanks')
    def thanks():
        return render_template('thanks.html')


    db.init_app(app)

    return app
