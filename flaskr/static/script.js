document.addEventListener("DOMContentLoaded", function(){
    key_idx = 0;
    prev_date = 0;
    durations = [];

    pair = $("#data-pair").text().trim();
    max_count = $("#data-max-counter").text().trim();

    function reset(){
      key_idx = 0;
      prev_date = 0;
      durations.length = 0;

      $("#counter").text(max_count);
      $("#input-pair").val("");

    }

    function updateCounter() {
      if (key_idx) {
        return;
      }
      c = $("#counter");
      c.text(c.text() - 1);
    }

    $('#input-pair').keypress(function(keyEvent){
        counter = $("#counter").text().trim();
        now = Date.now();

        if (counter == 0) {
            keyEvent.preventDefault();
            if (keyEvent.which == 13) {
              $.ajax({url: "/save",
                      type:"POST",
                      data: JSON.stringify(durations),
                      contentType:"application/json; charset=utf-8",
                      dataType:"json",
                      success: function( data ) {
                        console.log(data);
                        window.location = data;
                      }
              });
            }
            return;
        }
        if ( keyEvent.which == 13) {
          keyEvent.preventDefault();
        }


        if ( keyEvent.key != pair[key_idx]) {
             window.alert("Please try to type without mistakes");
             keyEvent.preventDefault();
             reset();
             return;
        }

        if ( prev_date != 0 && key_idx) {
            durations.push(now - prev_date);
        }
        key_idx = (key_idx + 1) % 2;
        prev_date = now;
        updateCounter();



    });


});
